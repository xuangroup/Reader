package com.kunfei.basemvplib;

import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.kunfei.basemvplib.impl.IPresenter;
import com.kunfei.basemvplib.impl.IView;
import com.monke.basemvplib.R;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import cn.bingoogolapple.swipebacklayout.BGASwipeBackHelper;

public abstract class BaseActivity<T extends IPresenter> extends AppCompatActivity implements IView, BGASwipeBackHelper.Delegate
{
    public final static String START_SHEAR_ELE = "start_with_share_ele";
    public static final int SUCCESS = 1;
    public static final int ERROR = -1;
    protected Bundle savedInstanceState;
    protected T mPresenter;
    protected boolean isRecreate;
    private Boolean startShareAnim = false;

    protected BGASwipeBackHelper mSwipeBackHelper;

    private void initSwipeBackFinish()
    {
        mSwipeBackHelper = new BGASwipeBackHelper(this, this);
        // 「必须在 Application 的 onCreate 方法中执行 BGASwipeBackHelper.init 来初始化滑动返回」
        // 下面几项可以不配置，这里只是为了讲述接口用法。
        // 设置滑动返回是否可用。默认值为 true
        mSwipeBackHelper.setSwipeBackEnable(true);
        // 设置是否仅仅跟踪左侧边缘的滑动返回。默认值为 true
        mSwipeBackHelper.setIsOnlyTrackingLeftEdge(false);
        // 设置是否是微信滑动返回样式。默认值为 true
        mSwipeBackHelper.setIsWeChatStyle(true);
        // 设置阴影资源 id。默认值为 R.drawable.bga_sbl_shadow
        mSwipeBackHelper.setShadowResId(R.drawable.bga_sbl_shadow);
        // 设置是否显示滑动返回的阴影效果。默认值为 true
        mSwipeBackHelper.setIsNeedShowShadow(true);
        // 设置阴影区域的透明度是否根据滑动的距离渐变。默认值为 true
        mSwipeBackHelper.setIsShadowAlphaGradient(true);
        // 设置触发释放后自动滑动返回的阈值，默认值为 0.3f
        mSwipeBackHelper.setSwipeBackThreshold(0.3f);
        // 设置底部导航条是否悬浮在内容上，默认值为 false
        mSwipeBackHelper.setIsNavigationBarOverlap(false);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        initSwipeBackFinish();
        super.onCreate(savedInstanceState);
        this.savedInstanceState = savedInstanceState;
        if (getIntent() != null)
        {
            isRecreate = getIntent().getBooleanExtra("isRecreate", false);
            startShareAnim = getIntent().getBooleanExtra(START_SHEAR_ELE, false);
        }
        AppActivityManager.getInstance().add(this);
        initSDK();
        onCreateActivity();
        mPresenter = initInjector();
        attachView();
        initData();
        bindView();
        bindEvent();
        firstRequest();
    }

    /**
     * 首次逻辑操作
     */
    protected void firstRequest()
    {

    }

    /**
     * 事件触发绑定
     */
    protected void bindEvent()
    {

    }

    /**
     * 控件绑定
     */
    protected void bindView()
    {

    }

    /**
     * P层绑定V层
     */
    private void attachView()
    {
        if (null != mPresenter)
        {
            mPresenter.attachView(this);
        }
    }

    /**
     * P层解绑V层
     */
    private void detachView()
    {
        if (null != mPresenter)
        {
            mPresenter.detachView();
        }
    }

    /**
     * SDK初始化
     */
    protected void initSDK()
    {

    }

    /**
     * P层绑定   若无则返回null;
     */
    protected abstract T initInjector();

    /**
     * 布局载入  setContentView()
     */
    protected abstract void onCreateActivity();

    /**
     * 数据初始化
     */
    protected abstract void initData();

    @Override
    protected void onResume()
    {
        super.onResume();
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        detachView();
        AppActivityManager.getInstance().remove(this);
    }

    @Override
    public void recreate()
    {
        getIntent().putExtra("isRecreate", true);
        super.recreate();
    }

    /////////Toast//////////////////

    public void toast(String msg)
    {
        toast(msg, Toast.LENGTH_SHORT, 0);
    }

    public void toast(String msg, int state)
    {
        toast(msg, Toast.LENGTH_LONG, state);
    }

    public void toast(int strId)
    {
        toast(strId, 0);
    }

    public void toast(int strId, int state)
    {
        toast(getString(strId), Toast.LENGTH_LONG, state);
    }

    public void toast(String msg, int length, int state)
    {
        Toast toast = Toast.makeText(this, msg, length);
        try
        {
            if (state == SUCCESS)
            {
                toast.getView().getBackground().setColorFilter(getResources().getColor(R.color.success), PorterDuff.Mode.SRC_IN);
            } else if (state == ERROR)
            {
                toast.getView().getBackground().setColorFilter(getResources().getColor(R.color.error), PorterDuff.Mode.SRC_IN);
            }
        } catch (Exception ignored)
        {
        }
        toast.show();
    }

    public Context getContext()
    {
        return this;
    }

    public Boolean getStart_share_ele()
    {
        return startShareAnim;
    }

    ////////////////////////////////启动Activity转场动画/////////////////////////////////////////////

    protected void startActivityForResultByAnim(Intent intent, int requestCode, int animIn, int animExit)
    {
        startActivityForResult(intent, requestCode);
        overridePendingTransition(animIn, animExit);
    }

    protected void startActivityByAnim(Intent intent, int animIn, int animExit)
    {
        startActivity(intent);
        overridePendingTransition(animIn, animExit);
    }

    protected void startActivityByAnim(Intent intent, @NonNull View view, @NonNull String transitionName, int animIn, int animExit)
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            intent.putExtra(START_SHEAR_ELE, true);
            ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(this, view, transitionName);
            startActivity(intent, options.toBundle());
        } else
        {
            startActivityByAnim(intent, animIn, animExit);
        }
    }

    @Override
    public boolean isSupportSwipeBack()
    {
        return false;
    }

    /**
     * 正在滑动返回
     *
     * @param slideOffset 从 0 到 1
     */
    @Override
    public void onSwipeBackLayoutSlide(float slideOffset)
    {
    }

    /**
     * 没达到滑动返回的阈值，取消滑动返回动作，回到默认状态
     */
    @Override
    public void onSwipeBackLayoutCancel()
    {
    }

    /**
     * 滑动返回执行完毕，销毁当前 Activity
     */
    @Override
    public void onSwipeBackLayoutExecuted()
    {
        mSwipeBackHelper.swipeBackward();
    }

    @Override
    public void onBackPressed()
    {
        // 正在滑动返回的时候取消返回按钮事件
        if (mSwipeBackHelper.isSliding())
        {
            return;
        }
        mSwipeBackHelper.backward();
    }
}